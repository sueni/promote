use std::ffi::OsStr;
use std::ffi::OsString;
use std::path::PathBuf;

pub struct Collisions {
    collisions: Vec<PathBuf>,
}

impl Collisions {
    pub fn new() -> Self {
        Self {
            collisions: Vec::new(),
        }
    }

    pub fn push(&mut self, path: PathBuf) {
        self.collisions.push(path);
    }

    pub fn report(&self) -> Option<()> {
        if self.collisions.is_empty() {
            return None;
        }
        eprintln!("Collisions:");
        for collision in &self.collisions {
            eprintln!("\t{}", collision.display())
        }
        Some(())
    }
}

pub struct TempCollision {
    content: Option<(PathBuf, PathBuf)>,
}

fn evolve(file_name: &OsStr) -> OsString {
    let mut os_string = file_name.to_os_string();
    os_string.push("%");
    os_string
}

impl TempCollision {
    pub fn new() -> Self {
        Self { content: None }
    }

    pub fn enclose(&mut self, current_path: PathBuf, desired_path: PathBuf) {
        let temp_file_name = evolve(desired_path.file_name().unwrap());
        let mut temp_path = desired_path.with_file_name(&temp_file_name);
        while temp_path.exists() {
            let temp_file_name = evolve(&temp_file_name);
            temp_path = desired_path.with_file_name(temp_file_name);
        }
        let temp_path = desired_path.with_file_name(temp_file_name);
        std::fs::rename(&current_path, &temp_path).unwrap();
        self.content = Some((temp_path, desired_path));
    }

    pub fn release(&mut self) {
        if let Some((temp_path, desired_path)) = &self.content {
            std::fs::remove_dir(&desired_path).unwrap();
            std::fs::rename(temp_path, desired_path).unwrap();
            self.content = None;
        }
    }
}
