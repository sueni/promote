use pico_args::Arguments;
use std::ffi::OsString;
use std::path::PathBuf;

pub const HELP: &str = "Usage: promo [-b: backup] [-f: overwrite] DIR(S)";

pub struct Opts {
    pub backup: bool,
    pub overwrite: bool,
    pub paths: Vec<PathBuf>,
}

pub fn parse() -> Result<Opts, String> {
    let mut args = Arguments::from_env();

    if args.contains(["-h", "--help"]) {
        return Err(HELP.to_string());
    }

    Ok(Opts {
        backup: args.contains("-b"),
        overwrite: args.contains("-f"),
        paths: parse_paths(args.finish())?,
    })
}

fn parse_paths(free: Vec<OsString>) -> Result<Vec<PathBuf>, String> {
    let paths: Vec<_> = free.into_iter().map(PathBuf::from).collect();

    if paths.is_empty() {
        return Err("Needs at leas one directory to operate on.".to_string());
    }

    if paths.iter().any(|s| !s.is_dir()) {
        return Err("Can only take existing directories as arguments.".to_string());
    }

    Ok(paths)
}
