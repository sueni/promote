use super::Promote;
use std::path::{Path, PathBuf};

pub struct PromoteSubdirsToPwd<'a> {
    sources: &'a [PathBuf],
    pwd: &'a Path,
}

impl<'a> PromoteSubdirsToPwd<'a> {
    pub fn new(sources: &'a [PathBuf], pwd: &'a Path) -> Self {
        Self { sources, pwd }
    }
}

impl<'a> Promote for PromoteSubdirsToPwd<'a> {
    fn sources(&self) -> &'a [PathBuf] {
        self.sources
    }

    fn target(&self) -> &Path {
        self.pwd
    }

    fn pwd(&self) -> &'a Path {
        self.pwd
    }

    fn remove_barren_dirs(&self) {
        for source in self.sources {
            if std::fs::remove_dir(source).is_err() {
                continue;
            }
        }
    }
}
