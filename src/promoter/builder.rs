use super::promote_pwd_overjump::PromotePwdOverjump;
use super::promote_pwd_up::PromotePwdUp;
use super::promote_subdirs_to_pwd::PromoteSubdirsToPwd;
use super::Promote;
use std::path::Path;
use std::path::PathBuf;

pub fn build<'a>(pwd: &'a Path, paths: &'a [PathBuf]) -> Option<Box<dyn Promote + 'a>> {
    if paths.iter().all(|s| s.parent().unwrap() == pwd) {
        return Some(Box::new(PromoteSubdirsToPwd::new(paths, pwd)));
    }

    if paths.len() == 1 {
        if paths[0] == pwd {
            return Some(Box::new(PromotePwdUp::new(paths, pwd)));
        }

        if let Some(common_ancestor) = promote::nonroot_common_ancestor(pwd, &paths[0]) {
            return Some(Box::new(PromotePwdOverjump::new(
                paths,
                pwd,
                common_ancestor,
            )));
        }
    }

    None
}
