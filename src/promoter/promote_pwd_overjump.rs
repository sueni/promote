use super::Promote;
use std::path::{Path, PathBuf};

pub struct PromotePwdOverjump<'a> {
    _sources: Vec<PathBuf>,
    target: &'a Path,
    pwd: &'a Path,
    common_ancestor: PathBuf,
}

impl<'a> PromotePwdOverjump<'a> {
    pub fn new(paths: &'a [PathBuf], pwd: &'a Path, common_ancestor: PathBuf) -> Self {
        let _sources = vec![pwd.to_path_buf()];
        let target = &paths[0];

        Self {
            _sources,
            target,
            pwd,
            common_ancestor,
        }
    }
}

impl<'a> Promote for PromotePwdOverjump<'a> {
    fn sources(&self) -> &[PathBuf] {
        self._sources.as_slice()
    }

    fn target(&self) -> &'a Path {
        self.target
    }

    fn pwd(&self) -> &'a Path {
        self.pwd
    }

    fn remove_barren_dirs(&self) {
        let source = &self.pwd;
        for ancestor in source.ancestors() {
            if ancestor == self.common_ancestor {
                break;
            }
            if std::fs::remove_dir(ancestor).is_err() {
                break;
            }
        }
    }
}
