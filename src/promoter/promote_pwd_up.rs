use super::Promote;
use std::path::{Path, PathBuf};

pub struct PromotePwdUp<'a, 'b> {
    sources: &'b [PathBuf],
    pwd: &'a Path,
}

impl<'a, 'b> PromotePwdUp<'a, 'b> {
    pub fn new(sources: &'b [PathBuf], pwd: &'a Path) -> Self {
        Self { sources, pwd }
    }
}

impl<'a, 'b> Promote for PromotePwdUp<'a, 'b> {
    fn sources(&self) -> &[PathBuf] {
        self.sources
    }

    fn target(&self) -> &'a Path {
        self.pwd.parent().unwrap()
    }

    fn pwd(&self) -> &'a Path {
        self.pwd
    }

    fn remove_barren_dirs(&self) {
        if self.pwd.is_dir() {
            let _ = std::fs::remove_dir(&self.pwd);
        }
    }
}
