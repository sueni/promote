pub mod builder;
mod promote_pwd_overjump;
mod promote_pwd_up;
mod promote_subdirs_to_pwd;

use crate::cli::Opts;
use crate::collisions::Collisions;
use crate::collisions::TempCollision;
use std::fs;
use std::path::{Path, PathBuf};

pub trait Promote {
    fn sources(&self) -> &[PathBuf];
    fn target(&self) -> &Path;
    fn pwd(&self) -> &Path;
    fn remove_barren_dirs(&self);

    fn report_collisions(&self, collisions: &Collisions) -> Option<()> {
        collisions.report()
    }

    fn move_content(&self, opts: &Opts, collisions: &mut Collisions) {
        let mut temp_collision = TempCollision::new();
        for dir in self.sources() {
            for item in fs::read_dir(dir).unwrap() {
                let item = item.unwrap();
                let mut destination = self.target().join(item.path().file_name().unwrap());
                if destination.exists() {
                    if &destination == dir {
                        temp_collision.enclose(item.path(), destination);
                        continue;
                    }
                    if opts.backup {
                        let mut counter = 0;
                        let path = item.path();
                        let ext = path.extension().unwrap_or_default();
                        while destination.exists() {
                            let file_stem = path.file_stem().unwrap();
                            let mut try_filename = file_stem.to_owned();
                            try_filename.push("~");
                            try_filename.push(counter.to_string());
                            try_filename.push(".");
                            try_filename.push(ext);
                            destination = self.target().join(try_filename);
                            counter += 1;
                        }
                    } else if !opts.overwrite {
                        collisions.push(destination);
                        continue;
                    }
                }
                // NOTE: won't work across devices
                fs::rename(item.path(), destination).unwrap();
            }
        }
        temp_collision.release();
    }

    fn action(&self, opts: &Opts, collisions: &mut Collisions) {
        self.move_content(opts, collisions);

        if self.report_collisions(collisions).is_none() {
            if self.target() != self.pwd() {
                println!("{}", self.target().display());
            }
            self.remove_barren_dirs();
        } else {
            std::process::exit(1);
        }
    }
}
