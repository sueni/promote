mod cli;
mod collisions;
mod promoter;

use crate::collisions::Collisions;
use std::path::PathBuf;

fn fail(msg: &str) -> ! {
    eprintln!("{}", msg);
    std::process::exit(1)
}

fn main() {
    let opts = match cli::parse() {
        Ok(v) => v,
        Err(e) => fail(&e),
    };

    let pwd = std::env::current_dir().unwrap();
    let paths: Vec<PathBuf> = opts
        .paths
        .iter()
        .map(|p| p.canonicalize().unwrap())
        .collect();

    if let Some(promoter) = promoter::builder::build(&pwd, &paths) {
        let mut collisions = Collisions::new();
        promoter.action(&opts, &mut collisions);
    } else {
        fail("Failed to pick the promoter.");
    };
}
