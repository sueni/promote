use std::path::{Path, PathBuf};

pub fn nonroot_common_ancestor(p1: &Path, p2: &Path) -> Option<PathBuf> {
    let mut ancestor = PathBuf::new();
    let p1_components = p1.components();
    let mut p2_components = p2.components();
    for p1 in p1_components {
        match p2_components.next() {
            Some(p2) => {
                if p1 == p2 {
                    ancestor.push(p1);
                } else {
                    break;
                }
            }
            None => break,
        }
    }
    if ancestor == Path::new("/") {
        return None;
    }
    Some(ancestor)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_nonroot_common_ancestor1() {
        let p1 = Path::new("/a/b/c/d");
        let p2 = Path::new("/a/b/1/2");
        assert_eq!(nonroot_common_ancestor(p1, p2), Some(PathBuf::from("/a/b")));
    }

    #[test]
    fn test_nonroot_common_ancestor2() {
        let p1 = Path::new("/a/b");
        let p2 = Path::new("/a/b/1/2");
        assert_eq!(nonroot_common_ancestor(p1, p2), Some(PathBuf::from("/a/b")));
    }

    #[test]
    fn test_nonroot_common_ancestor3() {
        let p1 = Path::new("/1/2");
        let p2 = Path::new("/a/b/c");
        assert_eq!(nonroot_common_ancestor(p1, p2), None);
    }
}
